<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

    protected $table = 'tags';
    protected $fillable = ['name', 'tag_type_id'];

    public function type()
    {
        return $this->belongsTo(TagType::class);
    }
}