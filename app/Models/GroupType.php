<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupType extends Model {

    protected $table = 'group_types';
    protected $fillable = ['name'];

    const TEAM = 1;
    const PROJECT = 2;
}