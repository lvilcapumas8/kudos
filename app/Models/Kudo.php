<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kudo extends Model {
    use HasFactory;

    protected $table = 'kudos';
    protected $fillable = [
        'type',
        'receivable_id',
        'receivable_type',
        'message',
        'tag_id',
        'sender_id'
    ];

    const TEAM = 1;
    const PROJECT = 2;

    public function receivable()
    {
        return $this->morphTo();
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}