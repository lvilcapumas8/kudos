<?php

namespace App\Models;

use App\Models\Group;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laravel\Lumen\Auth\Authorizable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable, HasFactory;

    protected $fillable = [
        'name', 'email',
    ];

    protected $hidden = [
        'password',
    ];

    public function kudo()
    {
        return $this->morphMany(Kudo::class, 'receivable');
    }

    public function kudos()
    {
        return $this->hasMany(Kudo::class, 'sender_id', 'id');
    }

    public function groups()
    {
        return $this->belongsToMany(Group::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    static function withFilter(Request $request)
    {
        return self::query()
            ->nameLike($request->name)
            ->orderBy('name');
    }

    public function scopeNameLike($query, $name)
    {
        $query->where('name', 'ilike', '%'.$name.'%');
    }
}
