<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Group extends Model {

    protected $table = 'groups';
    protected $fillable = ['name', 'group_type_id'];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function type()
    {
        return $this->belongsTo(GroupType::class, 'group_type_id', 'id');
    }

    public function kudo()
    {
        return $this->morphMany(Kudo::class, 'receivable');
    }

    static function withFilter(Request $request)
    {
        return self::query()
            ->nameLike($request->name)
            ->orderBy('name');
    }

    public function scopeNameLike($query, $name)
    {
        $query->where('name', 'ilike', '%'.$name.'%');
    }
}