<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TagType extends Model {

    protected $table = 'tag_types';
    protected $fillable = ['name'];

    const BIRTHDAY = 1;
    const WORK = 2;
    const GOAL = 3;
    const THANKS = 3;
}