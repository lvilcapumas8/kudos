<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function customPagination(LengthAwarePaginator $paginate, array $data = [])
    {
        $pag = $paginate->toArray();

        return [
            'data' => Arr::pull($pag, 'data'),
            'pagination' => $pag
        ] + $data;
    }
}
