<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Kudo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KudoController extends Controller
{
    public function list()
    {
        $kudos = Kudo::with([
            'receivable',
            'tag',
            'sender',
        ])->orderBy('created_at', 'desc')
        ->paginate(20);

        collect($kudos->items())->map(function ($kudo) {
            $receiver = $kudo->receivable;
            $kudo['receiver_type'] = $receiver->type->name ?? 'User';
            return $kudo;
        });

        return $this->customPagination($kudos);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'receiver_id' => 'required',
                'receiver_type' => 'required',
                'message' => 'required'
            ]);

            $receiver = $this->getReceiver($request->receiver_id, $request->receiver_type);
            $kudo = Kudo::create([
                'type' => 'public',
                'receivable_id' => $receiver->id,
                'receivable_type' => get_class($receiver),
                'message' => $request->message,
                'tag_id' => $request->tag_id,
                'sender_id' => Auth::user()->id,
            ]);

            return $kudo;

        } catch (\Exception $e) {
            return response()->json([
                'error' => $e->getMessage()
            ], 422);
        }
    }

    public function score()
    {
        $user = User::find(Auth::id());
        return [
            'sent' => $user->kudos()->count(),
            'received' => $user->kudo()->count(),
            'groups' =>  Kudo::where('receivable_type', 'App\\Models\\Group')
                ->whereIn('receivable_id', $user->groups()->pluck('id'))->count()
        ];
    }

    private function getReceiver($id, $type)
    {
        if ($type == 'User') {
            return User::find($id);
        }
        return Group::find($id);
    }
}
