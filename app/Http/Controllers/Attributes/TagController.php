<?php

namespace App\Http\Controllers\Attributes;

use App\Http\Controllers\Controller;
use App\Models\Tag;

class TagController extends Controller
{
    public function list()
    {
        return Tag::get(['id', 'name']);
    }
}
