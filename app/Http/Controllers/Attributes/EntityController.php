<?php

namespace App\Http\Controllers\Attributes;

use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\GroupType;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EntityController extends Controller
{
    public function list(Request $request)
    {
        $users = User::withFilter($request)
            ->selectRaw('id, name, null as group_type_id')
            ->where('id', '<>', Auth::user()->id);
        $groups = Group::withFilter($request)
            ->select(['id', 'name', 'group_type_id']);
        $entities = $users->unionAll($groups)->paginate(20);

        collect($entities->items())->map(function ($entity) {
            $entity['type'] = GroupType::find($entity->group_type_id)->name ?? 'User';
            return $entity;
        });

        return $this->customPagination($entities);
    }
}
