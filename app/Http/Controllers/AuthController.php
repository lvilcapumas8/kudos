<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|string',
                'password' => 'required',
            ]);

            $credentials = $request->only(['email', 'password']);
    
            if (!$token = Auth::setTTL(env('TOKEN_EXPIRATION_MINS', 600))
                ->attempt($credentials)) {
                return response()->json(['code' => 'UNAUTHORIZED'], 401);
            }

            $user = User::find(Auth::user()->id);
    
            return response()->json([
                'user' => $user->only(['id', 'name', 'email']),
                'token' => $token,
                'token_type' => 'Bearer',
                'expires_in' => Auth::factory()->getTTL() * 60
            ], 200);

        } catch (ValidationException $e) {
            return response()->json([
                'code' => 'VALIDATION_FAILED',
                'fields' => $e->getMessage()
            ], 201);

        } catch (\Exception $e) {
            return response()->json([
                'code' => 'LOGIN_FAILED',
                'message' => $e->getMessage()
            ], 409);
        }
    }

    /**
     * Register a new user
     *
     * @param  Request  $request
     * @return Response
     */
    public function register(Request $request)
    {
        try {
            $this->validate($request, [
                'name' => 'required|string',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
            ]);

            $user = new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $plainPassword = $request->input('password');
            $user->password = app('hash')->make($plainPassword);

            $user->save();

            return response()->json([
                'user' => $user,
                'message' => 'CREATED'
            ], 201);

        } catch (ValidationException $e) {
            return response()->json([
                'code' => 'VALIDATION_FAILED',
                'fields' => $e->getMessage()
            ], 201);

        } catch (\Exception $e) {
            return response()->json([
                'code' => 'REGISTRATION_FAILED',
                'message' => $e->getMessage()
            ], 409);
        }
    }

    public function logout()
    {
        try {
            Auth::logout(true);
        } catch (\Exception $exception) {
            response()->json([
                'code'=> 'LOGGED_OUT',
                'message' => $exception->getMessage()
            ], 200);
        }
    }
}