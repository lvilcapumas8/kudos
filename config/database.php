<?php

return [
    'default' => env('DB_CONNECTION'),
    'migrations' => 'migrations',
    'connections' => [
        'pgsql' => [
            'driver' => 'pgsql',
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', ''),
            'username' => env('DB_USERNAME', ''),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],
        'testing' => [
            'driver' => 'pgsql',
            'host' => env('TESTING_DB_HOST', '127.0.0.1'),
            'port' => env('TESTING_DB_PORT', '5432'),
            'database' => env('TESTING_DB_DATABASE', ''),
            'username' => env('TESTING_DB_USERNAME', ''),
            'password' => env('TESTING_DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'schema' => 'public',
            'sslmode' => 'prefer',
        ]
    ]
];