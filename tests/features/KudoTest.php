<?php

use App\Models\Group;
use App\Models\User;
use Illuminate\Support\Facades\Artisan;
use Laravel\Lumen\Testing\DatabaseMigrations;

class KudoTest extends TestCase
{
    use DatabaseMigrations;

    public function test_send_kudo()
    {
        Artisan::call('db:seed');
        
        //Arrange
        $url = route('home.kudo.store');
        $user = User::factory()->create();
        $this->actingAs($user, 'api');

        //Act
        $this->post($url, [
            "receiver_id" => 1,
            "receiver_type" => "User",
            "message" => "message",
            "tag_id" => 1
        ]);

        //Assert
        $this->assertResponseStatus(200);

        $this->assertArrayHasKey('message', $this->response->json());
        $this->assertArrayHasKey('sender_id', $this->response->json());
        $this->assertArrayHasKey('id', $this->response->json());
    }
}
