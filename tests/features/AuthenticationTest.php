<?php

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthenticationTest extends TestCase
{
    use DatabaseMigrations;

    public function test_register_new_user()
    {
        //Arrange
        $url = route('auth.register');

        //Act
        $this->post($url, [
            'name' => 'Miguel',
            'email' => 'miguel.vilcapuma@tektonlabs.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        //Assert
        $this->assertResponseStatus(201);
        $this->assertEquals(1, User::count());

        $user = User::first();

        $this->assertEquals('Miguel', $user->name);
        $this->assertEquals('miguel.vilcapuma@tektonlabs.com', $user->email);
        $this->assertTrue(Hash::check('password', $user->password));
    }

    public function test_login_user()
    {
        //Arrange
        $urlRegister = route('auth.register');
        $urlLogin = route('auth.login');

        //Act
        $this->post($urlRegister, [
            'name' => 'Miguel',
            'email' => 'miguel.vilcapuma@tektonlabs.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $this->post($urlLogin, [
            'email' => 'miguel.vilcapuma@tektonlabs.com',
            'password' => 'password'
        ]);

        //Assert
        $this->assertResponseStatus(200);
        $this->assertArrayHasKey('token', $this->response->json());
        $this->assertArrayHasKey('token_type', $this->response->json());
        $this->assertArrayHasKey('expires_in', $this->response->json());
    }
}
