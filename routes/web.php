<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'auth'], function () use ($router) {
        $router->post('register', ['uses' => 'AuthController@register', 'as' => 'auth.register']);
        $router->post('login', ['uses' => 'AuthController@login', 'as' => 'auth.login']);
        $router->post('logout', ['uses' => 'AuthController@logout', 'as' => 'auth.logout']);
    });

    $router->group(['middleware' => ['auth']], function ($router) {
        $router->group([
            'prefix' => 'attributes', 'namespace' => 'Attributes',
        ], function () use ($router) {
            $router->get('entities', ['uses' => 'EntityController@list', 'as' => 'attributes.entity.list']);
            $router->get('tags', ['uses' => 'TagController@list', 'as' => 'attributes.tag.list']);
        });

        $router->group([
            'prefix' => 'kudos', 'namespace' => 'Home',
        ], function () use ($router) {
            $router->post('store', ['uses' => 'KudoController@store', 'as' => 'home.kudo.store']);
            $router->get('list', ['uses' => 'KudoController@list', 'as' => 'home.kudo.list']);
            $router->get('score', ['uses' => 'KudoController@score', 'as' => 'home.kudo.score']);
        });
    });
});
