
# Kudos

## Project setup locally - Docker
### Building
```
docker-compose build
```
```
docker-compose up
```

### Go to PHP Container
```
docker exec -it kudos_php_1 bash
```

## Project setup - Lumen
### Install dependencies
```
composer install
```

### Migration and seed - DB
```
php artisan migrate:fresh --seed
```

### Environment
```
cp .env.example .env
```

### Generate secret key - Jwt
```
php artisan jwt:secret
```