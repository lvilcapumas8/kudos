<?php

namespace Database\Factories;

use App\Models\Group;
use App\Models\Kudo;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class KudoFactory extends Factory
{
    protected $model = Kudo::class;

    public function definition()
    {
        return [
            'type' => $this->faker->randomElement(['public', 'private']),
            'message' => $this->faker->text,
            'sender_id' => function () {
                $user = User::all()->random(1)->first();
                if (!$user) {
                    $user = $this->factory(User::class)->create();
                }
                
                return $user;
            },
            'tag_id' => function () {
                $tag = Tag::all()->random(1)->first();
                if (!$tag) {
                    $tag = $this->factory(Tag::class)->create();
                }
                
                return $tag;
            },
        ];
    }

    public function configure()
    {
        return $this->afterMaking(function (Kudo $kudo) {
            $receiverClasses = [User::class, Group::class];
            $receiverClass = $this->faker->randomElement($receiverClasses);
            
            $receiver = $receiverClass::all()->random(1)->first();
            
            $kudo->receivable_id = $receiver->id;
            $kudo->receivable_type = $receiverClass;
        });
    }
}
