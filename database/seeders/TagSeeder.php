<?php

namespace Database\Seeders;

use App\Models\Tag;
use App\Models\TagType;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    public function run()
    {
        $tags = [
            [
                'id' => 1,
                'name' => 'Feliz cumpleaños',
                'tag_type_id' => TagType::BIRTHDAY
            ],
            [
                'id' => 2,
                'name' => 'Buen trabajo',
                'tag_type_id' => TagType::BIRTHDAY
            ],
            [
                'id' => 3,
                'name' => 'Tú puedes',
                'tag_type_id' => TagType::GOAL
            ],
            [
                'id' => 4,
                'name' => 'Muchas gracias',
                'tag_type_id' => TagType::THANKS
            ]
        ];

        foreach ($tags as $tag) {
            KDSeeder::updateOrCreate(Tag::class, ['id' => $tag['id']], $tag);
        }
    }
}
