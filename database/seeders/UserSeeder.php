<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Osito Cariñosito',
            'email' => 'miguel.vilcapuma@tektonlabs.com',
            'password' => Hash::make('password')
        ]);

        $user->groups()->attach([1, 2]);
    }
}
