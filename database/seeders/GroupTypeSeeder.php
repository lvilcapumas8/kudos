<?php

namespace Database\Seeders;

use App\Models\GroupType;
use Illuminate\Database\Seeder;

class GroupTypeSeeder extends Seeder
{
    public function run()
    {
        $types = [
            [
                'id' => 1,
                'name' => 'Team',
            ],
            [
                'id' => 2,
                'name' => 'Project'
            ]
        ];

        foreach ($types as $type) {
            KDSeeder::updateOrCreate(GroupType::class, ['id' => $type['id']], $type);
        }
    }
}
