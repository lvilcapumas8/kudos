<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\GroupType;
use Illuminate\Database\Seeder;

class GroupSeeder extends Seeder
{
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'name' => 'People',
                'group_type_id' => GroupType::TEAM,
            ],
            [
                'id' => 2,
                'name' => 'Commercial Officer',
                'group_type_id' => GroupType::TEAM
            ],
            [
                'id' => 3,
                'name' => 'Web development',
                'group_type_id' => GroupType::TEAM

            ],
            [
                'id' => 4,
                'name' => 'Operations Officer',
                'group_type_id' => GroupType::TEAM
            ],
            [
                'id' => 5,
                'name' => 'Finance',
                'group_type_id' => GroupType::TEAM
            ],
            [
                'id' => 6,
                'name' => 'UX/UI Designer',
                'group_type_id' => GroupType::TEAM
            ],
            [
                'id' => 7,
                'name' => 'La grama',
                'group_type_id' => GroupType::PROJECT
            ],
        ];

        foreach ($groups as $group) {
            KDSeeder::updateOrCreate(Group::class, ['id' => $group['id']], $group);
        }
    }
}
