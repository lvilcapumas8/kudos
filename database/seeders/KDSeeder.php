<?php

namespace Database\Seeders;

class KDSeeder
{
    public static function updateOrCreate($class, array $attributes = [], array $values = [])
    {
        $item = $class::firstWhere($attributes);
        unset($values['id']);

        if ($item) {
            $item->update($values);
        } else {
            $class::create($values);
        }
    }
}
