<?php

namespace Database\Seeders;

use App\Models\TagType;
use Illuminate\Database\Seeder;

class TagTypeSeeder extends Seeder
{
    public function run()
    {
        $types = [
            [
                'id' => 1,
                'name' => 'birthday',
            ],
            [
                'id' => 2,
                'name' => 'work'
            ],
            [
                'id' => 3,
                'name' => 'goals'
            ],
            [
                'id' => 4,
                'name' => 'thanks'
            ]
        ];

        foreach ($types as $type) {
            KDSeeder::updateOrCreate(TagType::class, ['id' => $type['id']], $type);
        }
    }
}
